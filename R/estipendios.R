library(tidyverse)
library(ggrepel)

d <- read.csv2("data/recibos.tsv", sep = "\t", header = F, stringsAsFactors = F, comment.char = "#")  %>% 
   mutate(fecha = as.Date(V1, "%Y-%m-%d"),
          monto = str_remove_all(V3, "\\.") %>% str_replace(",", "\\.") %>% as.numeric())

# Datos Nico + Pau ####

d1 <- d %>% rename(descripcion = V3) %>% 
  select(descripcion, fecha, monto) %>% 
  mutate(fecha = as.Date(fecha, "%Y-%m-%d")) %>% 
  mutate(descripcion = ifelse(str_detect(descripcion, "COM"), "compl", descripcion)) %>% 
  mutate(descripcion = ifelse(str_detect(descripcion, "^LIQ.*BEC"), "liq", descripcion)) %>% 
  filter(descripcion != "compl") %>% 
  as_tibble()

d1 %>% pull(descripcion) %>% unique()

d2 <- read_tsv("data/recibos2.txt", col_names = F, comment = "#") %>% 
  mutate(X3 = paste0(X3, "-01")) %>% 
  mutate(fecha = as.Date(X3, "%Y-%m-%d")) %>% 
  rename(monto = X2, descripcion = X1) %>% select(-X3) %>% 
  mutate(descripcion = ifelse(str_detect(descripcion, "COM"), "compl", descripcion)) %>% 
  mutate(descripcion = ifelse(str_detect(descripcion, "LIQ.*BEC|Liq.*Bec"), "liq", descripcion)) %>% 
  filter(descripcion != "compl")

d2 %>% pull(descripcion) %>% unique()

dd <- bind_rows(d1, d2, .id = "persona") %>% 
  mutate(year = format(fecha, "%Y") %>% as.numeric) %>% 
  mutate(mes = format(fecha, "%m") %>% as.numeric) %>% 
  mutate(fecha = format(fecha, "%m-%d"))

# Datos dolar ####

tc <- read_tsv(comment = "#", file = "data/tipo_de_cambio.txt", 
               col_names = c("fecha", "precio"), 
               col_types = "cn") %>% 
  mutate(fecha = str_replace(pattern = "(\\d\\d)-(\\d\\d)-(\\d\\d)", 
                             string = fecha, 
                             replacement = "20\\3-\\2-\\1")) %>% 
  mutate(fecha = as.Date(fecha, "%Y-%m-%d")) %>% 
  mutate(year = format(fecha, "%Y") %>% as.numeric) %>% 
  mutate(mes = format(fecha, "%m") %>% as.numeric) %>% 
  group_by(year, mes) %>% 
  summarise(dolar_mensual = median(precio, na.rm = T))

tc2 <- readxl::read_xls("data/com3500_hasta2022.xls", skip = 2)
tc2 <- tc2[,1:2]
names(tc2) <- c("fecha", "precio")

tc2 <- tc2 %>%
  # mutate(fecha = str_replace(pattern = "(\\d\\d)-(\\d\\d)-(\\d\\d)",
  #                            string = fecha,
  #                            replacement = "20\\3-\\2-\\1")) %>%
  # mutate(fecha = as.Date(fecha, "%Y-%m-%d")) %>%
  mutate(year = format(fecha, "%Y") %>% as.numeric) %>%
  mutate(mes = format(fecha, "%m") %>% as.numeric) %>%
  group_by(year, mes) %>%
  summarise(dolar_mensual = median(precio, na.rm = T))

tc <- tc2

# normalize by USD ####

ddd <- left_join(dd, tc, by = c("year", "mes")) %>% 
  mutate(monto_usd = round(monto / dolar_mensual))



# Plot ARS ####

p <- ddd %>% 
  mutate(fecha = paste(year, str_pad(mes, width = 2))) %>% 
  ggplot(aes(x = fecha, y = monto)) + 
  geom_point() + 
  scale_y_continuous(trans = "log2") +
  ylab("monto en ARS (log2 scaled)") +
  ggtitle("Evolucion del estipendio en ARS (log2 scale)") +
  theme(axis.text.x = element_text(angle = 90))

p

p <- ddd %>% 
  mutate(fecha = paste(year, str_pad(mes, width = 2))) %>% 
  ggplot(aes(x = fecha, y = monto_usd)) + 
  geom_point() + 
  # scale_y_continuous(trans = "log2") + ylab("monto (log2 scale only)") +
  theme(axis.text.x = element_text(angle = 90)) + 
  ggtitle("Evolucion del estipendio en USD",
          "USD bcra.gob.ar/PublicacionesEstadisticas/Tipos_de_cambios.asp")

p
# plotly::ggplotly(p)

log_trans01 <- scales::log_trans(base = 1.1)

p1 <- dd %>% filter(year < 2020) %>% 
  ggplot(aes(x = fecha, y = monto, group = persona)) + 
  geom_point(aes(text = descripcion, color = persona)) + 
  geom_line(aes(text = descripcion, color = persona)) + 
  geom_label_repel(aes(label = monto), point.padding = 1, min.segment.length = 0, 
                   size = 3, max.iter = 100, force = 2) +
  facet_wrap(~year) +
  # scale_y_continuous(trans = "log2") +
  coord_trans(y = scales::log_trans(1.1)) + 
  ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p1

p1.1 <- dd %>% 
  ggplot(aes(x = fecha, y = monto)) + 
  geom_line(aes(text = descripcion, color = factor(year), group = interaction(persona, year), linetype = persona)) + 
  # geom_label_repel(aes(label = monto), point.padding = 1, min.segment.length = 0) +
  # facet_wrap(~year) +
  # scale_y_continuous(trans = "log2") +
  # coord_trans(y = scales::log_trans(1.1)) + 
  # ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p1.1
# plotly::ggplotly(p1.1)

p1.2 <- dd %>% 
  group_by(year, persona) %>% arrange(year, mes) %>% 
  mutate(monto_rel = monto / first(monto)) %>% 


  ggplot(aes(x = fecha, y = monto_rel)) + 
  geom_line(aes(
                group = interaction(persona, year), 
                color = factor(year), 
                linetype = persona,
                text = descripcion
                )) + 
  # geom_label_repel(aes(label = monto), point.padding = 1, min.segment.length = 0) +
  # facet_wrap(~year) +
  # scale_y_continuous(trans = "log2") +
  # coord_trans(y = scales::log_trans(1.1)) + 
  # ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p1.2
# plotly::ggplotly(p1.2)

# Plot USD ####

p2 <- ddd %>% filter(year < 2020) %>% 
  ggplot(aes(x = fecha, y = monto_usd, group = persona)) + 
  geom_point(aes(text = descripcion, color = persona)) + 
  geom_line(aes(text = descripcion, color = persona)) + 
  geom_label_repel(aes(label = monto_usd), 
                   point.padding = 1,
                   min.segment.length = 0,
                   alpha = 1, size = 3) +
  facet_wrap(~year) +
  coord_trans(y = scales::log_trans(1.1)) + # scale_y_continuous(trans = "log2") +
  ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p2

# plotly::ggplotly(p)


p2.1 <- ddd %>% 
  ggplot(aes(x = fecha, y = monto_usd)) + 
  geom_line(aes(text = descripcion, color = factor(year), group = interaction(persona, year), linetype = persona)) + 
  # geom_label_repel(aes(label = monto), point.padding = 1, min.segment.length = 0) +
  # facet_wrap(~year) +
  # scale_y_continuous(trans = "log2") +
  # coord_trans(y = scales::log_trans(1.1)) + 
  # ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p1.1
# plotly::ggplotly(p2.1)

p2.2 <- ddd %>% 
  group_by(year, persona) %>% arrange(year, mes) %>% 
  mutate(monto_rel = monto_usd / first(monto_usd)) %>% 
  
  
  ggplot(aes(x = fecha, y = monto_rel)) + 
  geom_line(aes(
    group = interaction(persona, year), 
    color = factor(year), 
    linetype = persona,
    text = descripcion
  )) + 
  # geom_label_repel(aes(label = monto), point.padding = 1, min.segment.length = 0) +
  # facet_wrap(~year) +
  # scale_y_continuous(trans = "log2") +
  # coord_trans(y = scales::log_trans(1.1)) + 
  # ylab("monto (log1.1 scale only)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45))

p1.2
# plotly::ggplotly(p2.2)
